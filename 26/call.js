/*
 * @Author: 遥航
 * @Date: 2022-01-26 12:29:33
 * @LastEditors: 遥航
 * @LastEditTime: 2022-01-28 12:48:33
 * @FilePath: \call.js
 * @Description: 
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-01-26 12:29:33
 */

// call与apply的区别
// --两者可以改变this指向,效果相同,区别在与传入参数形式的不同
// ---apply接受两个参数,第一个参数置顶了函数体内this对象额指向,第二个参数为一个带下标的集合,这个
//    集合可以为数组也可以是一个类数组,apply将这个集合中的元素作为参数传给被调用的函数
// ---call方法传入的参数数量不固定,跟apply相同的是,第一个参数也是规定函数体内的this指向,从第二个参数开始,每个参数都依次传入函数

var name = 'window';
var person = {
    name: 'person',
}
var doSth = function(){
    console.log(this.name);
    return function(){
        console.log('return:', this.name);
    }
}
var Student = {
    name: '若川',
    doSth: doSth,
}

doSth(); // 1. 写出结果和原因
// undefind  因为23行的this指向dosth函数,该函数没有name属性
Student.doSth(); // 2. 写出结果和原因
// 若川  Student中的dosth函数为  dosth函数,该函数保留Student的this指向,所以打印若川
Student.doSth.call(person); // 3. 写出结果和原因
// person call改变this指向,将原本指向Student的this指向了person,所以打印的name是20行的name
new Student.doSth.call(person);  // // 4. 写出结果和原因
//不执行  stydent不是构造函数 不可以使用new操作符

var student = {
  name: '若川',
  doSth: function(){
      console.log(this.name);
      return () => {
          console.log('arrowFn:', this.name);
      }
  }
}
var person = {
  name: 'person',
}

student.doSth().call(person); // 5. 写出结果和原因
// 若川  arrowFn: 若川  
// 执行doSth方法  打印this.name===>若川   ===>arrow 若川   之后call改变this指向,并未执行
student.doSth.call(person)(); // 6. 写出结果和原因
//person  arrowFn: person
//先改变this指向  再去执行函数