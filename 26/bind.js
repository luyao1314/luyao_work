/*
 * @Author: 遥航
 * @Date: 2022-01-26 12:20:41
 * @LastEditors: 遥航
 * @LastEditTime: 2022-01-26 12:27:28
 * @FilePath: \bind.js
 * @Description: 
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-01-26 12:20:41
 */

// .bind的用途主要是为了修改函数的this指向,
// bind 通过函数来包装func函数,并且传入一个对象context当作参数,这个context对象就是
//想要修正的this对象

//手动实现

Function.prototype.myBind = function () {
  let self = this  //保存原函数
  context = [].shift.call(arguments)  //需要绑定的this上下文
  args = [].slice.call(arguments); //将剩余的参数转化为数组
  return function () {  //返回一个新的方法
    return self.apply(context, [].concat.call(args, [].slice.call(arguments)))
    //执行新的函数时,会将之前传入的context当作新函数体内的this
  }
}