/*
 * @Author: 遥航
 * @Date: 2022-01-28 14:26:01
 * @LastEditors: 遥航
 * @LastEditTime: 2022-01-28 14:42:27
 * @FilePath: \emit.js
 * @Description: 
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-01-28 14:26:01
 */
class EventEmitter {
  static events = {}
  on(eventName, listener) {
    if (!this.events[eventName]) {
      this.events[eventName] = [listener]
    } else {
      this.events[eventName].push(listener)
    }
    return this
  }

  once(eventName, listener) {
    const on = (...args) => listener(...args);
    on.fn = listener
    this.on(eventName, on)
    return this
  }

  emit(eventName, args) {
    if (!this.events[eventName]) return false
    this.events[eventNameF].forEach((item) => {
      item(...args)
    })
    return true
  }

  remove(eventName, listener) {
    const fns = this.events[eventName]
    if (!fns) {   //如果事件不存在  直接返回
      return false
    }

    if (!listener) {  //如果没有传入具体参数,则表示需要取消eventName所有订阅
      fns && (fns.length = 0)
    } else {
      for (let i = fns.length - 1; i >= 0; i--) {
        let _fn = fns[i]
        if (_fn === listener) {
          fns.splice(i, 1)
        }
      }
    }
  }
}

//  发布订阅模式又称作观察者模式,它定义对象间的一种一对多的依赖关系,当一个对象的状态发生变化时,所有依赖于它的对象都将会得到通知,在js中,一半用事件模型来代替传统的发布订阅模式

//  作用
// -- 发布订阅模式广泛应用与异步编程中,可以替代传递回调函数的方案,使用者无需过多的关注对象的异步运行期间的内部状态
// -- 可以让两个对象松耦合地联系在一起,虽然不太清楚彼此的细节,但不影响他们之间的互相通信