/*
 * @Author: 遥航
 * @Date: 2022-01-28 14:46:30
 * @LastEditors: 遥航
 * @LastEditTime: 2022-01-28 15:03:41
 * @FilePath: \clone.js
 * @Description: 
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-01-28 14:46:30
 */

//浅拷贝与深拷贝主要针对引用数据类型
//因为引用数据类型占内存较大,所以是存放与栈内存中,会向js中抛出一个指针,指向内存位置
//浅拷贝只是拷贝第一层数据的指针,并不会创造新的栈内存进行存储,而深拷贝是将对象中的属性全部递归拷贝



let obj1 = {
  name: 'IT协会',
  age: 'the Tenth',
  sex: ['male', 'female']
}

//模拟浅拷贝  

// ---- Object.assign({}, obj1)  也是只能实现浅拷贝

function shallowClone(prpo) {
  let temp = {};           //let 一个临时空对象
  for (let i in prpo) {     //利用for...in函数进行遍历对象
    temp[i] = prpo[i];
  }
  return temp;             //返回temp对象
}

//模拟深拷贝
function Person(Obj) {
  return JSON.parse(JSON.stringify(Obj))
}