/*
 * @Author: 遥航
 * @Date: 2022-01-27 14:11:58
 * @LastEditors: 遥航
 * @LastEditTime: 2022-01-27 14:17:49
 * @FilePath: \debounce.js
 * @Description: 
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-01-27 14:11:58
 */
function debounce(func, wait, immediate) {
  let timeout = null;
  return function () {
    let context = this;
    // 保存调用事件回调函数时传入的参数（event对象）
    let args = arguments;
    if (timeout) clearTimeout(timeout);
    if (immediate) {
      let callNow = !timeout;
      timeout = setTimeout(function () {
        timeout = null;
      }, wait)
      if (callNow) {
        return func.apply(context, args);
      }
    } else {
      timeout = setTimeout(function () {
        func.apply(context, args)
      }, wait);
    }
  }
}
