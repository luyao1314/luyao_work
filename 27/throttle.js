/*
 * @Author: 遥航
 * @Date: 2022-01-27 14:18:52
 * @LastEditors: 遥航
 * @LastEditTime: 2022-01-27 14:22:39
 * @FilePath: \throttle.js
 * @Description: 
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-01-27 14:18:52
 */

function throttle(fn, wait, options) {
  let timer = null;
  return function () {
    let context = this;
    let args = arguments;
    if (options && !timer) {
      fn.apply(context, args)
    }
    if (!timer) {
      timer = setTimeout(function () {
        fn.apply(context, args);
        timer = null;
      }, wait)
    }
  }
}