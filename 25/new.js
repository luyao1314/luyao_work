/*
 * @Author: 遥航
 * @Date: 2022-01-25 10:28:52
 * @LastEditors: 遥航
 * @LastEditTime: 2022-01-25 11:46:29
 * @FilePath: \message.jsx
 * @Description:
 * Copyright 2022 OBKoro1, All Rights Reserved.
 * 2022-01-25 14:28:52
 */

function objectFactory() {
  let newObj = null   //创建一个空对象进行承接
  let constructor = Array.prototype.shift.call(arguments);
  let result = null;
  //首先判断new传入参数,是否为一个构造器
  if (typeof constructor !== "function") {
    console.error("type error"); //如果不是一个构造器,则直接报错
    return;
  }

  newObj = Object.create(constructor.prototype);

  //这里将this指向,指向新抛出的对象,并且将参数进行对应的赋值

  result = constructor.apply(newObj, arguments);

  let flag = result && (typeof result == "object" || typeof result == "function")

  return flag ? result : newObj
  //判断是否进行修改,返回新对象
}