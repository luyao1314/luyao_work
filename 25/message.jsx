/*
 * @Author: 遥航
 * @Date: 2022-01-25 10:28:52
 * @LastEditors: 遥航
 * @LastEditTime: 2022-01-25 11:46:29
 * @FilePath: \message.jsx
 * @Description:
 * Copyright 2022 OBKoro1, All Rights Reserved.
 * 2022-01-25 14:28:52
 */
class MessageCom extends Component {
  render() {
    return (
      <span style={{ color: this.props.color }}>
                    {this.props.text}
                
      </span>
    );
  }
}

export default function (obj) {
  const { type, text } = obj;

  if (type === "success") {
    return <MessageCom text={text} color="green" />;
  } else if (type === "fail") {
    return <MessageCom text={text} color="red" />;
  }
}
