/*
 * @Author: 遥航
 * @Date: 2022-01-29 10:55:52
 * @LastEditors: 遥航
 * @LastEditTime: 2022-01-29 11:04:26
 * @FilePath: \Objcreate.js
 * @Description: 
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-01-29 10:55:52
 */


//面对对象继承

function Animal(name) {
  this.name = name;
  this.getName = () => {
    return this.name
  }
}

function Dog(name, age) {
  Animal.call(this, name)
  this.age = age
}

let P = new Dog("旺财", "15")
console.log(P.name);