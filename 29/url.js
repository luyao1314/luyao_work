/*
 * @Author: 遥航
 * @Date: 2022-01-29 11:10:30
 * @LastEditors: 遥航
 * @LastEditTime: 2022-01-29 11:13:49
 * @FilePath: \url.js
 * @Description: 
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-01-29 11:10:30
 */
function parseParam(url) {
  let arr = url.split('?')[1].split("&")
  let obj = {}
  arr.forEach((item) => {
    obj[item.split("=")[0]] = item.split("=")[1]
  })
  return obj
}
console.log(parseParam("http://www.baidu.com?name=yihang&password=123456"));
