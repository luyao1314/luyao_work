//改写promise.all方法
let myall = (arr) => {
  let results = [];  //接受返回结果的数组
  let promiseCount = 0;  //判断所有请求是否请求完毕
  let promisesLength = arr.length;
  return new Promise((resolve, reject) => {
    for (let i = 0; i < arr.length; i++) {
      //遍历执行所有方法,正确执行then,将结果添加到数组中
      arr[i].then(res => {
        promiseCount++;
        results[i] = {
          state: "1001",
          data: res
        };
        // 当所有函数都正确执行了，resolve输出所有返回结果。
        if (promiseCount === promisesLength) {
          return resolve(results);
        }
      }).catch((err) => {  //执行失败执行catch方法,将结果添加到数组中
        promiseCount++;
        results[i] = {
          state: "1002",
          data: err
        };
        // 当所有函数都正确执行了，resolve输出所有返回结果。
        if (promiseCount === promisesLength) {
          return resolve(results);
        }
      })
    }
  });
};

let pro1 = new Promise(resolve => {
  resolve("第一个")
})

let pro2 = new Promise(resolve => {
  resolve("第二个")
})

let pro3 = new Promise((resolve, reject) => {
  reject("第三个")
})

myall([pro1, pro2, pro3]).then(res => {
  console.log(res, "========");
})
