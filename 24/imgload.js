/*
 * @Author: 遥航
 * @Date: 2022-01-23 16:13:10
 * @LastEditors: 遥航
 * @LastEditTime: 2022-01-23 16:28:02
 * @FilePath: \imgload.js
 * @Description: 
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-01-23 16:13:10
 */
let imgList = [...document.querySelectorAll('img')]

//所有图片渲染时,添加  data-src属性用来保存src路径
const imgLazyLoad = function () {
  let H = window.innerHeight  //获取页面的可视化高度
  let S = document.documentElement.scrollTop  //获取窗口滚动条距离
  for (let i = 0; i < imgList.length; i++) {

    //图片距离顶部的距离大于可视化窗口距离和滚动区域之和时,触发懒加载
    if ((H + S) > imgList[i].offsetTop) {
      imgList[i].src = imgList[i].getAttribute("data-src")
      imgList.splice(i) //完成懒加载之后,将图片移除
    }
  }

  if (imgList.length === 0) {
    document.removeEventListener(imgLazyLoad)  //如果所有图片加载完毕,清除监听事件
  }
}

document.addEventListener('scroll', imgLazyLoad)